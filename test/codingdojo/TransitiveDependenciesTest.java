package codingdojo;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import static org.junit.Assert.*;

public class TransitiveDependenciesTest {

    @Test
    public void testCalculateTransitiveDependencies_WorstCaseTimeComplexity() throws Exception{
        // Set size of graph here
        int n = 30;
        //int n = 1000;
        TransitiveDependencies worstCase = buildWorstCase(n);
        Set<String> solution = buildSolutionToWorstCase(n);
        long start = System.currentTimeMillis();
        assertEquals(solution, worstCase.calculateTransitiveDependencies("1"));
        long end = System.currentTimeMillis();
        System.out.println("n = " + n + ": Took " + (end-start) + " ms.");
    }

    /**
     * Worst case for time-complexity.
     *
     * Builds a graph of the form (already transitive):
     *
     * 1 -> 2, 3, 4, ..., n-1, n
     * 2 -> 3, 4, ..., n-1, n
     *   ...
     * n-1 -> n
     * n ->
     *
     * @param n the size
     * @return
     */
    private TransitiveDependencies buildWorstCase(int n) {
        TransitiveDependencies worstCaseGraph = new TransitiveDependencies();

        Stack<String> nodes = new Stack<>();
        for (int i = n; i>0; i--) {
            nodes.push(Integer.toString(i));
        }

        while (!nodes.isEmpty()) {
            worstCaseGraph.addDependency(nodes.pop(), nodes.toArray(new String[]{}));
        }

        return worstCaseGraph;
    }

    /**
     * Builds a set of all numbers (as Strings) from 2 to n.
     * @param n
     * @return
     */
    private Set<String> buildSolutionToWorstCase(int n) {

        Set<String> nodes = new HashSet<>();
        for (int i = 2; i<=n; i++) {
            nodes.add(Integer.toString(i));
        }

        return nodes;
    }

    @Test
    public void testCalculateTransitiveDependencies_MultipleConnectedSubgraphs() throws Exception{
        TransitiveDependencies testee = new TransitiveDependencies();

        testee.addDependency("A", "B", "C");
        testee.addDependency("B", "C");
        testee.addDependency("C", "D");

        testee.addDependency("E", "F");
        testee.addDependency("F", "G", "H");

        assertEquals(new HashSet<>(Arrays.asList("B", "C", "D")),testee.calculateTransitiveDependencies("A"));
        assertEquals(new HashSet<>(Arrays.asList("F", "G", "H")),testee.calculateTransitiveDependencies("E"));
    }

    @Test
    public void testCalculateTransitiveDependencies_simple() throws Exception{
        TransitiveDependencies testee = new TransitiveDependencies();
        testee.addDependency("A", "B", "C");
        testee.addDependency("B", "C");
        testee.addDependency("C", "D");
        testee.addDependency("D", "E", "F");
        assertEquals(new HashSet<>(Arrays.asList("B", "C", "D", "E", "F")),testee.calculateTransitiveDependencies("A"));
        assertEquals(new HashSet<>(Arrays.asList("C", "D", "E", "F")),testee.calculateTransitiveDependencies("B"));
    }
}