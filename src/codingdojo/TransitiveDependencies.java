package codingdojo;

import java.util.*;

/**
 * Coding Kata:
 * http://codekata.com/kata/kata18-transitive-dependencies/
 */
public class TransitiveDependencies {

    Map<String, String[]> graph = new HashMap<>();
    Map<String, Set<String>> transitiveGraph = new HashMap<>();

    public Set<String> calculateTransitiveDependencies(String s) {
        if (transitiveGraph.containsKey(s)) {
            // Remove next line for original complexity (2^n)
            return transitiveGraph.get(s);
        }

        String[] elements = graph.getOrDefault(s,new String[]{});
        Set<String> result = new HashSet<>(Arrays.asList(elements));

        for (String element : elements) {
            result.addAll(calculateTransitiveDependencies(element));
        }

        // Not exposing modifiable Set
        result = Collections.unmodifiableSet(result);

        // Saving result for further recursion-steps (dynamic programming)
        transitiveGraph.put(s, result);

        return result;
    }

    // TODO: Refactor (to builder pattern?), addDependency invalidates transitiveGraph!
    public void addDependency(String s, String... deps) {
        graph.put(s, deps);
    }
}
